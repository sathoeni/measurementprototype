//
//  MeasurementPrototypeAppDelegate.h
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 01.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MeasurementPrototypeViewController;

@interface MeasurementPrototypeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MeasurementPrototypeViewController *viewController;

@end
