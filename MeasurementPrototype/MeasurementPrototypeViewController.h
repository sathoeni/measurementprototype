//
//  MeasurementPrototypeViewController.h
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 01.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "MovementViewController.h"

@class AccelerometerViewController;

@interface MeasurementPrototypeViewController : UIViewController

@property (strong, nonatomic) MovementViewController *movementViewController;

- (IBAction)cameraButtonPressed:(id)sender;
@end
