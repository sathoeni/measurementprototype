//
//  MeasurementPrototypeViewController.m
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 01.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "MeasurementPrototypeViewController.h"

@interface MeasurementPrototypeViewController ()

@end

@implementation MeasurementPrototypeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //initialise the viewController
    self.movementViewController = [[MovementViewController alloc] initWithNibName:@"MovementViewController" bundle: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraButtonPressed:(id)sender {
    /*
    //UI Animations with default AnimationTransitions 
    //--------------------------------------------------------------------
    [UIView beginAnimations:@"View Flip" context:nil ];
    [UIView setAnimationDuration:1.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
     
     
     //[UIView commitAnimations];
    */
    
    //UI Animations with default Quartz CATransistions
    //--------------------------------------------------------------------
    //http://iphonedevwiki.net/index.php/CATransition
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 0.5f;
    transition.type =  @"cube";
    transition.subtype = @"fromRight";
    [self.view.layer removeAllAnimations];
    [self.view.layer addAnimation:transition forKey:kCATransition];
    
    
    //show the viewController
    [self.view addSubview:self.movementViewController.view];

}
@end
