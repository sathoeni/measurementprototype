//
//  MovementViewController.h
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 19.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import <CoreMotion/CoreMotion.h>
#import <Foundation/Foundation.h>



@interface MovementViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *xBar;
@property (weak, nonatomic) IBOutlet UIProgressView *yBar;
@property (weak, nonatomic) IBOutlet UIProgressView *zBar;
@property (weak, nonatomic) IBOutlet UILabel *xLabel;
@property (weak, nonatomic) IBOutlet UILabel *yLabel;
@property (weak, nonatomic) IBOutlet UILabel *zLabel;
@property (strong, nonatomic) IBOutlet UILabel *gravityLabelX;
@property (strong, nonatomic) IBOutlet UILabel *gravityLabelY;
@property (strong, nonatomic) IBOutlet UILabel *gravityLabelZ;
@property (strong, nonatomic) IBOutlet UIProgressView *gravityIndicatorX;
@property (strong, nonatomic) IBOutlet UIProgressView *gravityIndicatorY;
@property (strong, nonatomic) IBOutlet UIProgressView *gravityIndicatorZ;
@property (strong, nonatomic) IBOutlet UIProgressView *accelIndicatorX;
@property (strong, nonatomic) IBOutlet UIProgressView *accelIndicatorY;
@property (strong, nonatomic) IBOutlet UIProgressView *accelIndicatorZ;
@property (strong, nonatomic) IBOutlet UILabel *accelLabelX;
@property (strong, nonatomic) IBOutlet UILabel *accelLabelY;
@property (strong, nonatomic) IBOutlet UILabel *accelLabelZ;

@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property double distance;
@property double speedStart;
@property double speedEnd;
@property float updateIntervall;
@property bool isRunning;
@property (strong) NSOperationQueue *queue;
@property (strong) CMMotionManager *motionManager;
@property (strong) NSTimer *timer;

- (IBAction)closeViewButtonClicked:(id)sender;
- (IBAction)resetDistanceClicked:(id)sender;
- (IBAction)stopMeasureClicked:(id)sender;


@end
