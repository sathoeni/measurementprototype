//
//  main.m
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 01.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MeasurementPrototypeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MeasurementPrototypeAppDelegate class]));
    }
}
