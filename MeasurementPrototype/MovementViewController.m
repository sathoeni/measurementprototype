//
//  MovementViewController.m
//  MeasurementPrototype
//
//  Created by Sascha Thöni on 19.05.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "MovementViewController.h"

@interface MovementViewController ()

@end

@implementation MovementViewController

@synthesize xBar;
@synthesize yBar;
@synthesize zBar;
@synthesize xLabel;
@synthesize yLabel;
@synthesize zLabel;
@synthesize gravityIndicatorX;
@synthesize gravityIndicatorY;
@synthesize gravityIndicatorZ;
@synthesize gravityLabelX;
@synthesize gravityLabelY;
@synthesize gravityLabelZ;
@synthesize accelIndicatorX;
@synthesize accelIndicatorY;
@synthesize accelIndicatorZ;
@synthesize accelLabelX;
@synthesize accelLabelY;
@synthesize accelLabelZ;
@synthesize motionManager;
@synthesize distance;
@synthesize speedEnd;
@synthesize speedStart;
@synthesize distanceLabel;
@synthesize timer;
@synthesize updateIntervall;
@synthesize isRunning;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //set the size of the view to current device screen size
    self.view.frame = [UIScreen mainScreen].bounds;
    
    [self initController];  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) initController
{
    isRunning = TRUE;
    updateIntervall = 0.1f;
    
    motionManager = [[CMMotionManager alloc] init];
    motionManager.gyroUpdateInterval = 1.0/2.0;  // Update every 1/2 second.
    motionManager.accelerometerUpdateInterval = 1.0 / 100.0;
    [motionManager startDeviceMotionUpdates];
    if (motionManager.gyroAvailable) {
        NSLog(@"Gyroscope avaliable");
        timer = [NSTimer scheduledTimerWithTimeInterval:updateIntervall target:self selector:@selector(updateView:) userInfo:nil repeats:YES];
    } else {
        NSLog(@"Gyroscope not available");
    }
}

-(void) updateView:(NSTimer *)timer {
    
    //NSLog(@"Update View");
    
    //Get motion data from the device
    CMDeviceMotion *motionData = motionManager.deviceMotion;
    CMAttitude *attitude = motionData.attitude;
    CMAcceleration gravity = motionData.gravity;
    CMAcceleration userAcceleration = motionData.userAcceleration;
    CMRotationRate rotate = motionData.rotationRate;
    
    
    //count the distance in 1.0 / 100. second
    //if (motionData.userAcceleration.y > 0.05 || motionData.userAcceleration.y < -0.05) {
        
        //Speed and distance calculation
        //http://de.wikipedia.org/wiki/Gleichm%C3%A4%C3%9Fig_beschleunigte_Bewegung
        //-----------------------------------------------------
        
    if(motionData.userAcceleration.y < 0.02 ){ //&& motionData.userAcceleration.y > -0.02
        userAcceleration.y = 0.0;
    }
    if(isRunning)
    {
        
        //calculate the speed after a period
        double speedEndLocal = userAcceleration.y * updateIntervall + speedStart;
        
        double currentDistance = (userAcceleration.y / 2 * updateIntervall * updateIntervall) + (speedStart * updateIntervall) + distance;
        
        distance = currentDistance;
        speedStart = speedEndLocal;
        
                
        NSLog(@"Distance: %f", distance);
        NSLog(@"userAcceleration: %f", userAcceleration.y);
        //a solid move forward starts
        //lineLength++; //increment a line length value
    }
    NSString *distanceString = [NSString stringWithFormat:@"%f", distance];
    distanceLabel.text = distanceString;
    
   
    //Update the view (labels, progressbar etc...)
    
    /*
    xLabel.text = [NSString stringWithFormat:@"%f", rotate.x];
    xBar.progress = ABS(rotate.x);
    yLabel.text = [NSString stringWithFormat:@"%f", rotate.y];
    yBar.progress = ABS(rotate.y);
    zLabel.text = [NSString stringWithFormat:@"%f", rotate.z];
    zBar.progress = ABS(rotate.z);
    */
    
    gravityIndicatorX.progress = ABS(gravity.x);
    gravityIndicatorY.progress = ABS(gravity.y);
    gravityIndicatorZ.progress = ABS(gravity.z);
    gravityLabelX.text = [NSString stringWithFormat:@"%2.2f",gravity.x];
    gravityLabelY.text = [NSString stringWithFormat:@"%2.2f",gravity.y];
    gravityLabelZ.text = [NSString stringWithFormat:@"%2.2f",gravity.z];
    
    accelIndicatorX.progress = ABS(userAcceleration.x);
    accelIndicatorY.progress = ABS(userAcceleration.y);
    accelIndicatorZ.progress = ABS(userAcceleration.z);
    accelLabelX.text = [NSString stringWithFormat:@"%2.2f",userAcceleration.x];
    accelLabelY.text = [NSString stringWithFormat:@"%2.2f",userAcceleration.y];
    accelLabelZ.text = [NSString stringWithFormat:@"%2.2f",userAcceleration.z];
}


- (BOOL)shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait); }



- (IBAction)closeViewButtonClicked:(id)sender {

    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = 0.5f;
    transition.type =  @"cube";
    transition.subtype = @"fromLeft";
    [self.view.superview.layer removeAllAnimations];
    [self.view.superview.layer addAnimation:transition forKey:kCATransition];
    
    [self.view removeFromSuperview];
}

- (IBAction)resetDistanceClicked:(id)sender {
    distance = 0;
    speedStart = 0;
    isRunning = TRUE;
}

- (IBAction)stopMeasureClicked:(id)sender {
    isRunning = FALSE;
}

- (void)dealloc
{
    [motionManager stopGyroUpdates];
}

@end
